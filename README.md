SUB Docker SDK
==============

# Introduction

This is the SUB Docker SDK. It is aimed to smoothen the transition from development into deployment.

To achieve this goal by providing examples this (more or less) documentation project (or repository) 
is organized as Docker project itself. One might argue this it completely overengineered, but it's on purpose 
to cover a broad range of examples.

The contents of this SDK are available as Markdown files for reading and editing and can be displayed in 
a browser using the provided images. The contents are linked below to let one use the GitLab web frontend 
for reading.

There are several documented possibilities to contribute to these documents.

## Status

**This is far from being finished, don't use it yet unless you know what you're doing. There is no documentation yet. Issues won't get answed!**

## Contents

  - [Writing Dockerfiles]()

    - [General Considerations]()

  - [Deployment]()
  
  - [Contributing]()
  
  - [Frequently asked questions]()

## TODO

  -  Review https://github.com/exakat/php-static-analysis-tools

# Quickstart

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose up --pull --build -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -f docker/Dockerfile  .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


