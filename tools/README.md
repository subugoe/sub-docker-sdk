Tools
=====

This directory contains several tools supplied as Docker images.

# Selenium

For further documentation see the [README file](./selenium/README.md).

# Kubernetes deployer

For further documentation see the [README file](./k8s-deployer/README.md).