Solr with Geonames Data
=======================

This directory contains a Docker image running a Solr filled with GeoNames data.

# Quick start

To just use the image for searching place names run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker run -p 8983:8983 -d docker.gitlab.gwdg.de/subugoe/sub-docker-sdk/geonames:latest
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Building

You just need a recent docker version and some time to build the image:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t docker.gitlab.gwdg.de/subugoe/sub-docker-sdk/geonames:latest .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Downloading

Alternatively you just can download a pre build image. There should be build every night and contain the 
latest publicly available GeoNames data.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker pull docker.gitlab.gwdg.de/subugoe/sub-docker-sdk/geonames:latest
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Usage

To access the index you need to start a container and make it accessible fo your browser (argument `-p`) on a specific port.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker run -p 8983:8983 -d docker.gitlab.gwdg.de/subugoe/sub-docker-sdk/geonames:latest
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After the container has started, point your browser to [`http://localhost:8983`](http://localhost:8983).

## Index

The index is largely predefined by the structure of the [Geonames data dump](http://download.geonames.org/export/dump/readme.txt), 
the available fields are listed below.

### Fields

| Field name      | Geonames name           | Solr data type   | Description                                                        |
|-----------------|-------------------------|------------------|--------------------------------------------------------------------|
| `id`            | `geonameid`             | Unique `string`  | The ID of a data set                                               |
| `name`          | `name`                  | `string`         | The primary (english) name of a place                              |
| `alternatename` | `alternatenames`        | List of `string` | Variants of the name in different languages and at different times | 
| `coordinate`    | `latitude`, `longitude` | `location`       | The two original location fields merged into one                   |
| `featureclass`  | `feature class`         | `string`         | The [feature class](http://www.geonames.org/export/codes.html)     |
| `featurecode`   | `feature code`          | `string`         | The [feature code](http://www.geonames.org/export/codes.html)      |
| `country`       | `country`               | `string`         | The current country                                                |
| `population`    | `population`            | `pint`           | The population (if known)                                          |
| `elevation`     | `levation`              | `pint`           | The elevation (if known)                                           |
| `timezone`      | `timezone`              | `string`         | The timezone at the given place                                    |


### Additional field for name and it's variants

There is an additional field `n` which contains the primary (english) name, all variants and a normalized ASCII variant. 
The are the fields `name`, `asciiname`, `alternatenames` from Geonames.
One can use this to avoid querying multiple fields. But it's more likely to get false positives, especially for compound names.
It's advised to query this field only in combination with `featureclass` and `featuretype`.

## Queries

**Note:** Single parameter examples aren't URL encoded below.

### GeoJSON

Solr features a possibility to return GeoJSON as query result, you need to pass the parameter `wt=geojson` for the format and 
`geojson.field=coordinate` to let it kn ow which field contains the geo referenced information.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
http://localhost:8983/solr/geonames/select?wt=geojson&geojson.field=coordinate&q=name:G%C3%B6ttingen%20AND%20featureclass:P
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Bounding box

It's also possible to limit the search results to a bounding box by using the solr range notation on a field containing a location, 
using the `fq` parameter, like `fq=coordinate:[32.00,27.50 TO 43.00,37.50]`. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
http://localhost:8983/solr/geonames/select?fq=coordinate%3A%5B32.00%2C27.50%20TO%2043.00%2C37.50%5D&q=*%3A*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can use the web site [bboxfinder.com](http://bboxfinder.com/) to draw your own boxes.

### Additional geo aware queries

The are other query possibilities build into Solr, have a [look at the documentation](https://lucene.apache.org/solr/guide/8_2/spatial-search.html) 
to get some ideas. 

# Further possible enhancements

## Adding a XSLT stylesheet to return KML

One can use the [Solr XSLT ResponseWriter](https://lucene.apache.org/solr/guide/8_2/response-writers.html#xslt-response-writer) 
to create a XSLT stylesheet that converts the results to [KML](https://developers.google.com/kml/documentation/?csw=1).

## Export parts to reuse them in application specific indexes

It's possible to [export results](https://lucene.apache.org/solr/guide/8_2/exporting-result-sets.html) of a query into JSON 
this query can also contain a bounding box as shown above. Combining both techniques makes it easily possible to export and reindex 
parts of the whole Geonames corpus into a new core, index or database.
**Note:** You can also can use combined queries (see above) to create datasets based on population or featuretypes for a given region. 

### Using SQL

It's also possible to [export data using SQL clients](https://lucene.apache.org/solr/guide/8_2/parallel-sql-interface.html). 

### Using `wget` or `curl`

You can also use `wget` or `curl`. The following example exports GeoJSON for a given bounding box. Make sure you set the `row` parameter 
high enough to get all results. One might us `jq` for post processing.
 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
http://localhost:8983/solr/geonames/select?fq=coordinate%3A%5B28.00%2C32.00%20TO%2038.00%2C42.00%20%5D&q=*%3A*&wt=geojson&geojson.field=coordinate&start=0&rows=122000
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Using `jq` to get rid of the result wrapper:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
jq '.response'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Using `jq` to get rid of the result wrapper and `FeatureCollection`:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
jq '.response|.features'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Include more data on alternative names

There is data on alternatives names which indicates the language or timespans when a alternative name has been used, the table 
also includes alternative indentifiers for ressources, like wikidata IDs. These can be useful as well.